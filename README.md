# plotxAOD

A simple tool for visualizing and comparing the disk usage of xAODs

## Usage

First, you might want to add the executable to your path using
```
source setup.sh
```
Then to visualize, run
```
plotxAOD AOD1.pool.root AOD2.pool.root ... AODN.pool.root
```
which should make a bar plot that looks like this

![](./examples/figure.png)
