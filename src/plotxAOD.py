#!/usr/bin/env python

"""
Script to visualize the output of checkxAOD.py

Maybe you will find it useful, maybe not
"""

import argparse
import sys
import subprocess
import matplotlib.pyplot as plt
import numpy as np


def get_args():

    parser = argparse.ArgumentParser()

    parser.add_argument(
        'filenames',
        help='xAOD file(s) to visualize',
        nargs='*'
    )

    args = parser.parse_args()
    if not args.filenames and sys.stdin.isatty():
        parser.print_usage()
        sys.exit('ERROR: need to provide a path to valid xAOD file to plot')
    return args

def get_data_from_output(results):
    
    results = results.split('\n')

    containers = []
    disk_size  = []

    # find the CSV info
    for index, line in enumerate(results):
        if "CSV" in line:
            containers = results[index+1].split(',')
            disk_size  = map(float, results[index+2].split(','))
            fraction   = map(float, results[index+3].split(','))
            break

    return dict(zip(containers,disk_size))

def prepare_data(file_data):

    if len(file_data) == 1:
        return file_data

    all_containers = set().union(*file_data)
    empty_dict = dict.fromkeys(all_containers, 0.0)

    # generate new list of dictionaries with added empty keys
    file_data_prepped = [{**empty_dict, **d} for d in file_data]

    # identify the largest file, and sort dicts accordingly
    largest_file = max(file_data_prepped, key=lambda x:x['Total'])
    largest_file_sorted = dict(sorted(largest_file.items(), key=lambda item: item[1]))
    file_data_prepped = [ dict(sorted(ifile.items(), key=lambda kv: largest_file_sorted[kv[0]])) for ifile in file_data_prepped]

    return file_data_prepped

def plot_data(file_names,file_data):

    num_containers = len(file_data[0].values())

    x = np.arange(num_containers)    # the label locations
    width = 0.35  # the width of the bars

    fig, ax = plt.subplots()

    # stretch fig width
    wi, hi = fig.get_size_inches()
    fig.set_size_inches(wi*2,hi)

    bars = []
    for index,data in enumerate(file_data):
        bar = ax.bar(x + index*width/len(file_data), data.values(), width, label=file_names[index] + ': ' + str(data['Total']) + ' kb/evt') 
        bars.append(bar)

    # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_ylabel('Disk usage (kb/evt)')
    ax.set_title('Disk usage by container')
    ax.set_xticks(x)
    ax.set_xticklabels(file_data[0].keys())
    ax.legend()
    ax.set_yscale('log')

    fig.tight_layout()

    plt.savefig(
        'figure.png',
        format='png'
    )

def main():
    args = get_args()

    filenames = args.filenames
    file_data = []

    for fn in filenames:
      result = subprocess.run(['checkxAOD.py', fn], capture_output=True, text=True)
      file_data.append(get_data_from_output(result.stdout))

    prepped_data = prepare_data(file_data)
    plot_data(filenames,prepped_data)

if __name__ == "__main__":
    main()
